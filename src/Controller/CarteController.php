<?php

namespace App\Controller;

use App\Entity\Carte;
use App\Form\CarteType;
use App\Repository\CarteRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/carte')]
class CarteController extends AbstractController
{
    #[Route('/', name: 'app_carte_index', methods: ['GET'])]
    public function index(CarteRepository $carteRepository): Response
    { if($this->getUser()){
        return $this->render('carte/index.html.twig', [
            'cartes' => $carteRepository->findAll(),
        ]);}
        else{
            return $this->redirectToRoute('app_login', [], Response::HTTP_SEE_OTHER);
        }
    }

    #[Route('/new', name: 'app_carte_new', methods: ['GET', 'POST'])]
    public function new(Request $request,FileUploader $fileUploader, CarteRepository $carteRepository): Response
    {
        if($this->getUser()){
        $carte = new Carte();
        $form = $this->createForm(CarteType::class, $carte);
        $form->handleRequest($request);}
        else{
            return $this->redirectToRoute('app_login', [], Response::HTTP_SEE_OTHER);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $carte->setIdUser($this->getUser());
            $imageFile = $form->get('photo')->getData();
            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $carte->setPhoto($imageFileName);
            }
            $carteRepository->add($carte);
            return $this->redirectToRoute('app_carte_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('carte/new.html.twig', [
            'carte' => $carte,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_carte_show', methods: ['GET'])]
    public function show(Carte $carte): Response
    { if($this->getUser()){
        return $this->render('carte/show.html.twig', [
            'carte' => $carte,
        ]);}
        else{
            return $this->redirectToRoute('app_login', [], Response::HTTP_SEE_OTHER);
        }
    }

    #[Route('/{id}/edit', name: 'app_carte_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request,FileUploader $fileUploader, Carte $carte, CarteRepository $carteRepository): Response
    { if($this->getUser()){
        $form = $this->createForm(CarteType::class, $carte);
        $form->handleRequest($request);
    }
    else{
        return $this->redirectToRoute('index_index', [], Response::HTTP_SEE_OTHER);
    }

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form->get('photo')->getData();
            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $carte->setphoto($imageFileName);
            }
            $carteRepository->add($carte);
            return $this->redirectToRoute('app_carte_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('carte/edit.html.twig', [
            'carte' => $carte,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_carte_delete', methods: ['POST'])]
    public function delete(Request $request, Carte $carte, CarteRepository $carteRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$carte->getId(), $request->request->get('_token'))) {
            $carteRepository->remove($carte);
        }

        return $this->redirectToRoute('app_carte_index', [], Response::HTTP_SEE_OTHER);
    }
}
