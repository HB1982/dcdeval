<?php

namespace App\Controller;

use App\Entity\Infos;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\InfosRepository;
use App\Form\InfosType;



class InfosaccueilController extends AbstractController
{
    #[Route('/infosaccueil', name: 'app_infosaccueil')]
  
    public function index(InfosRepository $infosRepository): Response
    {
        return $this->render('infos/infosaccueil.html.twig', [
            'infos' => $infosRepository->findAll(),
        ]);
    }}