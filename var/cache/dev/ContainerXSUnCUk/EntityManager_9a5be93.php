<?php

namespace ContainerXSUnCUk;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder31e87 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerf2ae1 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties9541a = [
        
    ];

    public function getConnection()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getConnection', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getMetadataFactory', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getExpressionBuilder', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'beginTransaction', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getCache', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getCache();
    }

    public function transactional($func)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'transactional', array('func' => $func), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'wrapInTransaction', array('func' => $func), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'commit', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->commit();
    }

    public function rollback()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'rollback', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getClassMetadata', array('className' => $className), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'createQuery', array('dql' => $dql), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'createNamedQuery', array('name' => $name), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'createQueryBuilder', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'flush', array('entity' => $entity), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'clear', array('entityName' => $entityName), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->clear($entityName);
    }

    public function close()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'close', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->close();
    }

    public function persist($entity)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'persist', array('entity' => $entity), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'remove', array('entity' => $entity), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'refresh', array('entity' => $entity), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'detach', array('entity' => $entity), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'merge', array('entity' => $entity), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getRepository', array('entityName' => $entityName), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'contains', array('entity' => $entity), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getEventManager', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getConfiguration', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'isOpen', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getUnitOfWork', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getProxyFactory', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'initializeObject', array('obj' => $obj), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'getFilters', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'isFiltersStateClean', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'hasFilters', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return $this->valueHolder31e87->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerf2ae1 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder31e87) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder31e87 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder31e87->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, '__get', ['name' => $name], $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        if (isset(self::$publicProperties9541a[$name])) {
            return $this->valueHolder31e87->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder31e87;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder31e87;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder31e87;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder31e87;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, '__isset', array('name' => $name), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder31e87;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder31e87;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, '__unset', array('name' => $name), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder31e87;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder31e87;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, '__clone', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        $this->valueHolder31e87 = clone $this->valueHolder31e87;
    }

    public function __sleep()
    {
        $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, '__sleep', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;

        return array('valueHolder31e87');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerf2ae1 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerf2ae1;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerf2ae1 && ($this->initializerf2ae1->__invoke($valueHolder31e87, $this, 'initializeProxy', array(), $this->initializerf2ae1) || 1) && $this->valueHolder31e87 = $valueHolder31e87;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder31e87;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder31e87;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
