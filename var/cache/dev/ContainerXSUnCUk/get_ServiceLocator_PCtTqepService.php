<?php

namespace ContainerXSUnCUk;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_PCtTqepService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.PCtTqep' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.PCtTqep'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'carte' => ['privates', '.errored..service_locator.PCtTqep.App\\Entity\\Carte', NULL, 'Cannot autowire service ".service_locator.PCtTqep": it references class "App\\Entity\\Carte" but no such service exists.'],
        ], [
            'carte' => 'App\\Entity\\Carte',
        ]);
    }
}
