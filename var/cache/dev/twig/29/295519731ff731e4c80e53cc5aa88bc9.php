<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* header.html.twig */
class __TwigTemplate_c7e2103f711203f21ca7f9025bac5e4d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header.html.twig"));

        // line 1
        echo " 
 ";
        // line 2
        $this->displayBlock('javascripts', $context, $blocks);
        // line 5
        echo " 
 <div class=\"container\">
        <header>
            <div class=\"banniere\">
                       <img class=\"logo\" src=\"https://i.ibb.co/HFkcSgh/logo.png\" alt=\"logo du restaurant dead cow diner\">
                <p class=\"ouverture\">Ouvert du mardi au dimanche, de 12h à 14h et de 14h à 19h</p>
                <div class=\"marquee-rtl\">
                <div>Le restaurant est<span id=\"heure\" ></span></div>
                </div>
            </div>
            <div class=\"menu\"> 

               <ul>
             
                     <li>
  
                <a href='";
        // line 21
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_infosaccueil");
        echo "'>Infos</a>
                
            </li> 

            <li>
                <a href='";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_menu");
        echo "'>Carte</a>
            </li>

            <li>
                <a href='";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_reservation_new");
        echo "'>Réservation</a>
                
            </li>

                </ul>
            </div>

        </header>

";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 2
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 3
        echo "
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 3,  102 => 2,  82 => 30,  75 => 26,  67 => 21,  49 => 5,  47 => 2,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source(" 
 {% block javascripts %}

{% endblock %}
 
 <div class=\"container\">
        <header>
            <div class=\"banniere\">
                       <img class=\"logo\" src=\"https://i.ibb.co/HFkcSgh/logo.png\" alt=\"logo du restaurant dead cow diner\">
                <p class=\"ouverture\">Ouvert du mardi au dimanche, de 12h à 14h et de 14h à 19h</p>
                <div class=\"marquee-rtl\">
                <div>Le restaurant est<span id=\"heure\" ></span></div>
                </div>
            </div>
            <div class=\"menu\"> 

               <ul>
             
                     <li>
  
                <a href='{{ url('app_infosaccueil') }}'>Infos</a>
                
            </li> 

            <li>
                <a href='{{ url('app_menu') }}'>Carte</a>
            </li>

            <li>
                <a href='{{ url('app_reservation_new') }}'>Réservation</a>
                
            </li>

                </ul>
            </div>

        </header>

", "header.html.twig", "/var/www/html/symfony/DCD/templates/header.html.twig");
    }
}
