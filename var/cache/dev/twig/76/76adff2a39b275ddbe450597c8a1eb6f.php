<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* accueil/home.html.twig */
class __TwigTemplate_ddede12c26a382b2ea700097d35f57ee extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'javascripts' => [$this, 'block_javascripts'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "accueil/home.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "accueil/home.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "accueil/home.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "<link rel=\"stylesheet\" href='/css/style.css' />
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 9
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Carte index";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 11
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo " 
  <div class=\"container\">
 ";
        // line 14
        $this->loadTemplate("header.html.twig", "accueil/home.html.twig", 14)->display($context);
        // line 15
        echo "
 <div class=\"main\">
            <div class=\"index_text\">
                <p class=\"presentation\">
                    L’aventure est née de la rencontre de compétences différentes mais complémentaires. Voici notre histoire...
                </p>
                <button id=\"btn\" onclick=\"laFonction()\" class=\"btnpopup\">voir plus...</button>
               
            </div>
            <div id=\"popup\" class=\"popup_cache\">
                <div class=\"modal-content\">
                    <div class=\"contenupc\">

                    <p class=\"p1\">

                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia porta mi at ultricies. Fusce commodo eros mauris. Pellentesque facilisis risus eget tellus fringilla, at sagittis ex pretium. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. Aliquam egestas pretium lorem id fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam cursus, quam non commodo ultricies, nunc massa ultricies eros, at luctus est justo varius dolor. Donec sagittis mattis lacus. Aliquam vitae sapien ut libero interdum tincidunt nec vitae quam. In commodo, ante vitae sollicitudin porttitor, enim libero volutpat nisl, ac ornare felis sapien porta urna. Maecenas rhoncus dui eros, at vehicula quam malesuada et. Aenean sagittis odio ut ipsum mattis placerat.
                    </p>

                    <p class=\"p2\">

                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia porta mi at ultricies. Fusce commodo eros mauris. Pellentesque facilisis risus eget tellus fringilla, at sagittis ex pretium. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. Aliquam egestas pretium lorem id fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam cursus, quam non commodo ultricies, nunc massa ultricies eros, at luctus est justo varius dolor. Donec sagittis mattis lacus. Aliquam vitae sapien ut libero interdum tincidunt nec vitae quam. In commodo, ante vitae sollicitudin porttitor, enim libero volutpat nisl, ac ornare felis sapien porta urna. Maecenas rhoncus dui eros, at vehicula quam malesuada et. Aenean sagittis odio ut ipsum mattis placerat.
                    </p>
                    </div>
 <div class=\"contenumobile\">
               
                <img class=\"imghistoire\" src=\"\" alt=\"burger\">
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.

Nulla consectetur vulputate odio, eget laoreet libero mattis a. Ut tristique ligula quam, vel tincidunt turpis fringilla quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi nec interdum lectus. Nam aliquam sollicitudin ipsum, rhoncus pretium tellus. Quisque laoreet ex vel egestas congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

Sed vitae ultrices ligula, at mattis nisl. Nunc scelerisque sapien id lorem tristique, eu dictum mauris sagittis. Phasellus non iaculis justo, ac lacinia mi. Aliquam non condimentum dui, fringilla maximus mauris. Nunc bibendum libero in sollicitudin maximus. Ut efficitur tortor fringilla, interdum felis quis, luctus nulla. Proin porta eleifend justo, vitae tincidunt felis. Vivamus convallis orci urna, a consequat urna euismod eu. Aenean porta ac felis et ultricies. Donec vel sapien lectus. Duis eu augue ac urna euismod tincidunt. Nunc posuere posuere sodales.

Aenean imperdiet sed nisl vitae finibus. Integer nec purus id orci sodales pharetra a eget lacus. Duis sit amet est luctus, luctus velit id, imperdiet nibh. Nullam dictum, lacus in viverra congue, mauris ligula semper sem, eget scelerisque diam tellus at ex. Nullam nisl ante, bibendum rutrum pellentesque nec, pulvinar dictum elit. Phasellus id enim porttitor, hendrerit orci sed, tincidunt lectus. Aliquam consectetur, lorem eget cursus bibendum, turpis libero sodales lectus, ut tempor urna velit id tortor. Aliquam ut consequat arcu. Fusce eu iaculis massa. Nunc sed consectetur orci. Fusce nec dui.
</p>
<img class=\"imghistoire\" src=\"images/gens.jpg\" alt=\"gens\">
<p >
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.

Nulla consectetur vulputate odio, eget laoreet libero mattis a. Ut tristique ligula quam, vel tincidunt turpis fringilla quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi nec interdum lectus. Nam aliquam sollicitudin ipsum, rhoncus pretium tellus. Quisque laoreet ex vel egestas congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

Sed vitae ultrices ligula, at mattis nisl. Nunc scelerisque sapien id lorem tristique, eu dictum mauris sagittis. Phasellus non iaculis justo, ac lacinia mi. Aliquam non condimentum dui, fringilla maximus mauris. Nunc bibendum libero in sollicitudin maximus. Ut efficitur tortor fringilla, interdum felis quis, luctus nulla. Proin porta eleifend justo, vitae tincidunt felis. Vivamus convallis orci urna, a consequat urna euismod eu. Aenean porta ac felis et ultricies. Donec vel sapien lectus. Duis eu augue ac urna euismod tincidunt. Nunc posuere posuere sodales.

Aenean imperdiet sed nisl vitae finibus. Integer nec purus id orci sodales pharetra a eget lacus. Duis sit amet est luctus, luctus velit id, imperdiet nibh. Nullam dictum, lacus in viverra congue, mauris ligula semper sem, eget scelerisque diam tellus at ex. Nullam nisl ante, bibendum rutrum pellentesque nec, pulvinar dictum elit. Phasellus id enim porttitor, hendrerit orci sed, tincidunt lectus. Aliquam consectetur, lorem eget cursus bibendum, turpis libero sodales lectus, ut tempor urna velit id tortor. Aliquam ut consequat arcu. Fusce eu iaculis massa. Nunc sed consectetur orci. Fusce nec dui.
</p>

<img class=\"imghistoire\" src=\"images/gril.jpg\" alt=\"gril\">
<p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.


</p>
<img class=\"imghistoire\" src=\"images/table.jpg\" alt=\"gens\">
<p class=\"partie1\">

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.


</p>

<img class=\"imghistoire\" src=\"images/palo-duro-canyon.jpg\" alt=\"canyon\">
<p class=\"partie1\">

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.
</p>



<img class=\"imghistoire\" src=\"images/Panorama-sur-le-desert-Arches-Moab-Utah.jpg\" alt=\"pano\">

                </div>
                    <button id=\"close\" class=\"close\" onclick=\"Close()\">X</button>

                </div>
            </div>

 <div class=\"slide_container\">
                <div class=\"imgborder1 slider\"></div>
                <div class=\"imgborder2 slider\"></div>
                <div class=\"imgborder3 slider\"></div>
                <div class=\"imgborder4 slider\"></div>
                <div class=\"imgborder5 slider\"></div>
            </div>

        </div>

       
      
    </div>
 <div class=\"admin\"><a href= '";
        // line 114
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_login");
        echo "'>Administrateur</a></div>
<script src=\"/js/app.js\"></script>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "accueil/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 114,  135 => 15,  133 => 14,  129 => 12,  119 => 11,  100 => 9,  89 => 7,  79 => 6,  61 => 3,  38 => 2,);
    }

    public function getSourceContext()
    {
        return new Source(" 
 {% extends 'base.html.twig' %}
 {% block javascripts %}
{% endblock %}

{% block stylesheets %}
<link rel=\"stylesheet\" href='/css/style.css' />
{% endblock %}
{% block title %}Carte index{% endblock %}

{% block body %}
 
  <div class=\"container\">
 {% include 'header.html.twig'%}

 <div class=\"main\">
            <div class=\"index_text\">
                <p class=\"presentation\">
                    L’aventure est née de la rencontre de compétences différentes mais complémentaires. Voici notre histoire...
                </p>
                <button id=\"btn\" onclick=\"laFonction()\" class=\"btnpopup\">voir plus...</button>
               
            </div>
            <div id=\"popup\" class=\"popup_cache\">
                <div class=\"modal-content\">
                    <div class=\"contenupc\">

                    <p class=\"p1\">

                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia porta mi at ultricies. Fusce commodo eros mauris. Pellentesque facilisis risus eget tellus fringilla, at sagittis ex pretium. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. Aliquam egestas pretium lorem id fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam cursus, quam non commodo ultricies, nunc massa ultricies eros, at luctus est justo varius dolor. Donec sagittis mattis lacus. Aliquam vitae sapien ut libero interdum tincidunt nec vitae quam. In commodo, ante vitae sollicitudin porttitor, enim libero volutpat nisl, ac ornare felis sapien porta urna. Maecenas rhoncus dui eros, at vehicula quam malesuada et. Aenean sagittis odio ut ipsum mattis placerat.
                    </p>

                    <p class=\"p2\">

                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia porta mi at ultricies. Fusce commodo eros mauris. Pellentesque facilisis risus eget tellus fringilla, at sagittis ex pretium. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. Aliquam egestas pretium lorem id fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam cursus, quam non commodo ultricies, nunc massa ultricies eros, at luctus est justo varius dolor. Donec sagittis mattis lacus. Aliquam vitae sapien ut libero interdum tincidunt nec vitae quam. In commodo, ante vitae sollicitudin porttitor, enim libero volutpat nisl, ac ornare felis sapien porta urna. Maecenas rhoncus dui eros, at vehicula quam malesuada et. Aenean sagittis odio ut ipsum mattis placerat.
                    </p>
                    </div>
 <div class=\"contenumobile\">
               
                <img class=\"imghistoire\" src=\"\" alt=\"burger\">
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.

Nulla consectetur vulputate odio, eget laoreet libero mattis a. Ut tristique ligula quam, vel tincidunt turpis fringilla quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi nec interdum lectus. Nam aliquam sollicitudin ipsum, rhoncus pretium tellus. Quisque laoreet ex vel egestas congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

Sed vitae ultrices ligula, at mattis nisl. Nunc scelerisque sapien id lorem tristique, eu dictum mauris sagittis. Phasellus non iaculis justo, ac lacinia mi. Aliquam non condimentum dui, fringilla maximus mauris. Nunc bibendum libero in sollicitudin maximus. Ut efficitur tortor fringilla, interdum felis quis, luctus nulla. Proin porta eleifend justo, vitae tincidunt felis. Vivamus convallis orci urna, a consequat urna euismod eu. Aenean porta ac felis et ultricies. Donec vel sapien lectus. Duis eu augue ac urna euismod tincidunt. Nunc posuere posuere sodales.

Aenean imperdiet sed nisl vitae finibus. Integer nec purus id orci sodales pharetra a eget lacus. Duis sit amet est luctus, luctus velit id, imperdiet nibh. Nullam dictum, lacus in viverra congue, mauris ligula semper sem, eget scelerisque diam tellus at ex. Nullam nisl ante, bibendum rutrum pellentesque nec, pulvinar dictum elit. Phasellus id enim porttitor, hendrerit orci sed, tincidunt lectus. Aliquam consectetur, lorem eget cursus bibendum, turpis libero sodales lectus, ut tempor urna velit id tortor. Aliquam ut consequat arcu. Fusce eu iaculis massa. Nunc sed consectetur orci. Fusce nec dui.
</p>
<img class=\"imghistoire\" src=\"images/gens.jpg\" alt=\"gens\">
<p >
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.

Nulla consectetur vulputate odio, eget laoreet libero mattis a. Ut tristique ligula quam, vel tincidunt turpis fringilla quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi nec interdum lectus. Nam aliquam sollicitudin ipsum, rhoncus pretium tellus. Quisque laoreet ex vel egestas congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

Sed vitae ultrices ligula, at mattis nisl. Nunc scelerisque sapien id lorem tristique, eu dictum mauris sagittis. Phasellus non iaculis justo, ac lacinia mi. Aliquam non condimentum dui, fringilla maximus mauris. Nunc bibendum libero in sollicitudin maximus. Ut efficitur tortor fringilla, interdum felis quis, luctus nulla. Proin porta eleifend justo, vitae tincidunt felis. Vivamus convallis orci urna, a consequat urna euismod eu. Aenean porta ac felis et ultricies. Donec vel sapien lectus. Duis eu augue ac urna euismod tincidunt. Nunc posuere posuere sodales.

Aenean imperdiet sed nisl vitae finibus. Integer nec purus id orci sodales pharetra a eget lacus. Duis sit amet est luctus, luctus velit id, imperdiet nibh. Nullam dictum, lacus in viverra congue, mauris ligula semper sem, eget scelerisque diam tellus at ex. Nullam nisl ante, bibendum rutrum pellentesque nec, pulvinar dictum elit. Phasellus id enim porttitor, hendrerit orci sed, tincidunt lectus. Aliquam consectetur, lorem eget cursus bibendum, turpis libero sodales lectus, ut tempor urna velit id tortor. Aliquam ut consequat arcu. Fusce eu iaculis massa. Nunc sed consectetur orci. Fusce nec dui.
</p>

<img class=\"imghistoire\" src=\"images/gril.jpg\" alt=\"gril\">
<p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.


</p>
<img class=\"imghistoire\" src=\"images/table.jpg\" alt=\"gens\">
<p class=\"partie1\">

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.


</p>

<img class=\"imghistoire\" src=\"images/palo-duro-canyon.jpg\" alt=\"canyon\">
<p class=\"partie1\">

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.
</p>



<img class=\"imghistoire\" src=\"images/Panorama-sur-le-desert-Arches-Moab-Utah.jpg\" alt=\"pano\">

                </div>
                    <button id=\"close\" class=\"close\" onclick=\"Close()\">X</button>

                </div>
            </div>

 <div class=\"slide_container\">
                <div class=\"imgborder1 slider\"></div>
                <div class=\"imgborder2 slider\"></div>
                <div class=\"imgborder3 slider\"></div>
                <div class=\"imgborder4 slider\"></div>
                <div class=\"imgborder5 slider\"></div>
            </div>

        </div>

       
      
    </div>
 <div class=\"admin\"><a href= '{{ url('app_login') }}'>Administrateur</a></div>
<script src=\"/js/app.js\"></script>
{% endblock %}", "accueil/home.html.twig", "/var/www/html/symfony/DCD/templates/accueil/home.html.twig");
    }
}
