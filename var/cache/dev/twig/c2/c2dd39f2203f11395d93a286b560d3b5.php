<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* infos/infosaccueil.html.twig */
class __TwigTemplate_f5bd8f527f4adf02861da6be11a3f176 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "infos/infosaccueil.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "infos/infosaccueil.html.twig"));

        // line 1
        echo "
 <html>
    <head>
        <meta charset=\"UTF-8\">
         <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
    <link href=\"https://fonts.googleapis.com/css2?family=Libre+Baskerville&display=swap\" rel=\"stylesheet\">
    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
    <link href=\"https://fonts.googleapis.com/css2?family=Questrial&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href='/css/styleinfo.css'>

 <div class=\"navinfos\">
  <img class=\"logo\" src=\"https://i.ibb.co/HFkcSgh/logo.png\" alt=\"logo du restaurant dead cow diner\">
    <h1>Informations générales</h1>
</div>
    
  <div class=\"infos\">

        <div class=\"googlemap\">
            <iframe class=\"pc\"src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2909.219089684536!2d3.0114733151259623!3d43.18391357914022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1ac77508ce4eb%3A0xd555a1b79ae98dcc!2s43%20Av.%20Pr%C3%A9sident%20Kennedy%2C%2011100%20Narbonne!5e0!3m2!1sfr!2sfr!4v1637674263179!5m2!1sfr!2sfr\" width=\"850\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" alt=\"plan google map du restaurant\">
            </iframe>
           
        </div>

        <div class=\"coordonnees\">
            <div class=\"text\">
               
            </div>
            <div class=\"liste\">
   ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["infos"]) || array_key_exists("infos", $context) ? $context["infos"] : (function () { throw new RuntimeError('Variable "infos" does not exist.', 31, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
            // line 32
            echo "                <ul>
                     <li>";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "description", [], "any", false, false, false, 33), "html", null, true);
            echo "</li>
                    <li>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "adresse", [], "any", false, false, false, 34), "html", null, true);
            echo "</li>
                    <li><a href=\"tel:";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "telephone", [], "any", false, false, false, 35), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "telephone", [], "any", false, false, false, 35), "html", null, true);
            echo "</a></li>
                    <li><a href=\"mail:";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "mail", [], "any", false, false, false, 36), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "mail", [], "any", false, false, false, 36), "html", null, true);
            echo "</a></li>
                </ul>
            </div>
        </div>
    </div>

  ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 43
            echo "            <tr>
                <td colspan=\"6\">no records found</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "



  ";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "infos/infosaccueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 47,  111 => 43,  97 => 36,  91 => 35,  87 => 34,  83 => 33,  80 => 32,  75 => 31,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
 <html>
    <head>
        <meta charset=\"UTF-8\">
         <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
    <link href=\"https://fonts.googleapis.com/css2?family=Libre+Baskerville&display=swap\" rel=\"stylesheet\">
    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
    <link href=\"https://fonts.googleapis.com/css2?family=Questrial&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href='/css/styleinfo.css'>

 <div class=\"navinfos\">
  <img class=\"logo\" src=\"https://i.ibb.co/HFkcSgh/logo.png\" alt=\"logo du restaurant dead cow diner\">
    <h1>Informations générales</h1>
</div>
    
  <div class=\"infos\">

        <div class=\"googlemap\">
            <iframe class=\"pc\"src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2909.219089684536!2d3.0114733151259623!3d43.18391357914022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1ac77508ce4eb%3A0xd555a1b79ae98dcc!2s43%20Av.%20Pr%C3%A9sident%20Kennedy%2C%2011100%20Narbonne!5e0!3m2!1sfr!2sfr!4v1637674263179!5m2!1sfr!2sfr\" width=\"850\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" alt=\"plan google map du restaurant\">
            </iframe>
           
        </div>

        <div class=\"coordonnees\">
            <div class=\"text\">
               
            </div>
            <div class=\"liste\">
   {% for info in infos %}
                <ul>
                     <li>{{ info.description }}</li>
                    <li>{{ info.adresse }}</li>
                    <li><a href=\"tel:{{ info.telephone }}\">{{ info.telephone }}</a></li>
                    <li><a href=\"mail:{{ info.mail }}\">{{ info.mail }}</a></li>
                </ul>
            </div>
        </div>
    </div>

  {% else %}
            <tr>
                <td colspan=\"6\">no records found</td>
            </tr>
        {% endfor %}




  ", "infos/infosaccueil.html.twig", "/var/www/html/symfony/DCD/templates/infos/infosaccueil.html.twig");
    }
}
