<?php

namespace Container54aHGAh;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getInfosRepositoryService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Repository\InfosRepository' shared autowired service.
     *
     * @return \App\Repository\InfosRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Repository\\InfosRepository'] = new \App\Repository\InfosRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
