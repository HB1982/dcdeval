<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* accueil/home.html.twig */
class __TwigTemplate_b9d170b45d4c3e738f11dae89c51322a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'javascripts' => [$this, 'block_javascripts'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "accueil/home.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "<script src=\"/js/app.js\"></script>    
";
    }

    // line 7
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "<link rel=\"stylesheet\" href='/css/style.css' />
";
    }

    // line 10
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Carte index";
    }

    // line 12
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "  <section>
  <div class=\"container\">
 ";
        // line 15
        $this->loadTemplate("header.html.twig", "accueil/home.html.twig", 15)->display($context);
        // line 16
        echo "
 <div class=\"main\">
            <div class=\"index_text\">
                <p class=\"presentation\">
                    L’aventure est née de la rencontre de compétences différentes mais complémentaires. Voici notre histoire...
                </p>
                <button id=\"btn\" onclick=\"laFonction()\" class=\"btnpopup\">voir plus...</button>
               
            </div>
            <div id=\"popup\" class=\"popup_cache\">
                <div class=\"modal-content\">
                    <div class=\"contenupc\">

                    <p class=\"p1\">

                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia porta mi at ultricies. Fusce commodo eros mauris. Pellentesque facilisis risus eget tellus fringilla, at sagittis ex pretium. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. Aliquam egestas pretium lorem id fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam cursus, quam non commodo ultricies, nunc massa ultricies eros, at luctus est justo varius dolor. Donec sagittis mattis lacus. Aliquam vitae sapien ut libero interdum tincidunt nec vitae quam. In commodo, ante vitae sollicitudin porttitor, enim libero volutpat nisl, ac ornare felis sapien porta urna. Maecenas rhoncus dui eros, at vehicula quam malesuada et. Aenean sagittis odio ut ipsum mattis placerat.
                    </p>

                    <p class=\"p2\">

                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia porta mi at ultricies. Fusce commodo eros mauris. Pellentesque facilisis risus eget tellus fringilla, at sagittis ex pretium. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. Aliquam egestas pretium lorem id fermentum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam cursus, quam non commodo ultricies, nunc massa ultricies eros, at luctus est justo varius dolor. Donec sagittis mattis lacus. Aliquam vitae sapien ut libero interdum tincidunt nec vitae quam. In commodo, ante vitae sollicitudin porttitor, enim libero volutpat nisl, ac ornare felis sapien porta urna. Maecenas rhoncus dui eros, at vehicula quam malesuada et. Aenean sagittis odio ut ipsum mattis placerat.
                    </p>
                    </div>
 <div class=\"contenumobile\">
               
                <img class=\"imghistoire\" src=\"\" alt=\"burger\">
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.

Nulla consectetur vulputate odio, eget laoreet libero mattis a. Ut tristique ligula quam, vel tincidunt turpis fringilla quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi nec interdum lectus. Nam aliquam sollicitudin ipsum, rhoncus pretium tellus. Quisque laoreet ex vel egestas congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

Sed vitae ultrices ligula, at mattis nisl. Nunc scelerisque sapien id lorem tristique, eu dictum mauris sagittis. Phasellus non iaculis justo, ac lacinia mi. Aliquam non condimentum dui, fringilla maximus mauris. Nunc bibendum libero in sollicitudin maximus. Ut efficitur tortor fringilla, interdum felis quis, luctus nulla. Proin porta eleifend justo, vitae tincidunt felis. Vivamus convallis orci urna, a consequat urna euismod eu. Aenean porta ac felis et ultricies. Donec vel sapien lectus. Duis eu augue ac urna euismod tincidunt. Nunc posuere posuere sodales.

Aenean imperdiet sed nisl vitae finibus. Integer nec purus id orci sodales pharetra a eget lacus. Duis sit amet est luctus, luctus velit id, imperdiet nibh. Nullam dictum, lacus in viverra congue, mauris ligula semper sem, eget scelerisque diam tellus at ex. Nullam nisl ante, bibendum rutrum pellentesque nec, pulvinar dictum elit. Phasellus id enim porttitor, hendrerit orci sed, tincidunt lectus. Aliquam consectetur, lorem eget cursus bibendum, turpis libero sodales lectus, ut tempor urna velit id tortor. Aliquam ut consequat arcu. Fusce eu iaculis massa. Nunc sed consectetur orci. Fusce nec dui.
</p>
<img class=\"imghistoire\" src=\"images/gens.jpg\" alt=\"gens\">
<p >
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.

Nulla consectetur vulputate odio, eget laoreet libero mattis a. Ut tristique ligula quam, vel tincidunt turpis fringilla quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi nec interdum lectus. Nam aliquam sollicitudin ipsum, rhoncus pretium tellus. Quisque laoreet ex vel egestas congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

Sed vitae ultrices ligula, at mattis nisl. Nunc scelerisque sapien id lorem tristique, eu dictum mauris sagittis. Phasellus non iaculis justo, ac lacinia mi. Aliquam non condimentum dui, fringilla maximus mauris. Nunc bibendum libero in sollicitudin maximus. Ut efficitur tortor fringilla, interdum felis quis, luctus nulla. Proin porta eleifend justo, vitae tincidunt felis. Vivamus convallis orci urna, a consequat urna euismod eu. Aenean porta ac felis et ultricies. Donec vel sapien lectus. Duis eu augue ac urna euismod tincidunt. Nunc posuere posuere sodales.

Aenean imperdiet sed nisl vitae finibus. Integer nec purus id orci sodales pharetra a eget lacus. Duis sit amet est luctus, luctus velit id, imperdiet nibh. Nullam dictum, lacus in viverra congue, mauris ligula semper sem, eget scelerisque diam tellus at ex. Nullam nisl ante, bibendum rutrum pellentesque nec, pulvinar dictum elit. Phasellus id enim porttitor, hendrerit orci sed, tincidunt lectus. Aliquam consectetur, lorem eget cursus bibendum, turpis libero sodales lectus, ut tempor urna velit id tortor. Aliquam ut consequat arcu. Fusce eu iaculis massa. Nunc sed consectetur orci. Fusce nec dui.
</p>

<img class=\"imghistoire\" src=\"images/gril.jpg\" alt=\"gril\">
<p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.


</p>
<img class=\"imghistoire\" src=\"images/table.jpg\" alt=\"gens\">
<p class=\"partie1\">

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.


</p>

<img class=\"imghistoire\" src=\"images/palo-duro-canyon.jpg\" alt=\"canyon\">
<p class=\"partie1\">

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis aliquet lorem, id rutrum risus. Duis mollis faucibus magna ac tristique. Curabitur elementum, velit in cursus fermentum, tellus nunc malesuada tellus, vitae placerat libero arcu id lectus. Vestibulum tincidunt pharetra risus, id elementum lacus faucibus ac. Pellentesque euismod, sapien a convallis scelerisque, nibh urna auctor est, id tincidunt ligula dolor sit amet lectus. Duis gravida magna ut egestas euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Praesent hendrerit at mauris quis commodo. Mauris viverra massa nisl, et luctus lectus ultrices vel. Suspendisse tempus dui vitae orci convallis, ut finibus lacus volutpat. Praesent feugiat porta ultricies. Integer tincidunt dui sit amet egestas scelerisque. Quisque auctor lorem sit amet facilisis finibus. Duis dapibus orci arcu, nec dictum enim pulvinar eu. In vel efficitur dui. Sed pretium porttitor elit, nec gravida tellus mattis vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In hac habitasse platea dictumst. Nullam eu nunc purus.
</p>



<img class=\"imghistoire\" src=\"images/Panorama-sur-le-desert-Arches-Moab-Utah.jpg\" alt=\"pano\">

                </div>
                    <button id=\"close\" class=\"close\" onclick=\"Close()\">X</button>

                </div>
            </div>

 <div class=\"slide_container\">
                <div class=\"imgborder1 slider\"></div>
                <div class=\"imgborder2 slider\"></div>
                <div class=\"imgborder3 slider\"></div>
                <div class=\"imgborder4 slider\"></div>
                <div class=\"imgborder5 slider\"></div>
            </div>

        </div>

       
      
    </div>
 <div class=\"admin\"><a href= '";
        // line 115
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_login");
        echo "'>Administrateur</a></div>
</section>
";
    }

    public function getTemplateName()
    {
        return "accueil/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 115,  84 => 16,  82 => 15,  78 => 13,  74 => 12,  67 => 10,  62 => 8,  58 => 7,  53 => 5,  49 => 4,  38 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "accueil/home.html.twig", "/var/www/html/symfony/DCD/templates/accueil/home.html.twig");
    }
}
