<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* header.html.twig */
class __TwigTemplate_b43be1d18873e130022b89917bdc57ba extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo " <div class=\"container\">
        <header>
            <div class=\"banniere\">
                       <img class=\"logo\" src=\"https://i.ibb.co/HFkcSgh/logo.png\" alt=\"logo du restaurant dead cow diner\">
                <p class=\"ouverture\">Ouvert du mardi au dimanche, de 12h à 14h et de 14h à 19h</p>
                <div class=\"marquee-rtl\">

                    <div>Le restaurant est<span id=heure></span></div>
                </div>
            </div>
            <div class=\"menu\">
               <ul>
              ";
        // line 16
        echo "             <li>
  
  
                <a href='";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_infosaccueil");
        echo "'>Infos</a>
                
            </li> 
                 <li>
                <a href='";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_menu");
        echo "'>Carte</a>
            </li>
           <li>
                <a href='";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_reservation_new");
        echo "'>Réservation</a>
                
            </li>

                </ul>
            </div>

        </header>

";
    }

    public function getTemplateName()
    {
        return "header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 26,  63 => 23,  56 => 19,  51 => 16,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "header.html.twig", "/var/www/html/symfony/DCD/templates/header.html.twig");
    }
}
