<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* carte/listeproduits.html.twig */
class __TwigTemplate_df2fe1578fec44a1fe54bb467c3c98cb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "carte/listeproduits.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "<link rel=\"stylesheet\" href='/css/stylemenu.css' />

";
    }

    // line 8
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "<div class=\"navcarte\">
  <img class=\"logo\" src=\"https://i.ibb.co/HFkcSgh/logo.png\" alt=\"logo du restaurant dead cow diner\">
    <h1>Notre Carte</h1>
</div>
    
        
     <div class=\"menu\">
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cartes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["carte"]) {
            // line 17
            echo "                <div class=\"unecarte\">
                
                <div class=\"nom\">";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["carte"], "nom", [], "any", false, false, false, 19), "html", null, true);
            echo "</div>
                <div class=\"imgmenu\"><img src= ";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/" . twig_get_attribute($this->env, $this->source, $context["carte"], "photo", [], "any", false, false, false, 20))), "html", null, true);
            echo " alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["carte"], "nom", [], "any", false, false, false, 20), "html", null, true);
            echo "\" style=\"width:80%; height:150px\" /img></div>
                
                <div class=\"description\">";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["carte"], "description", [], "any", false, false, false, 22), "html", null, true);
            echo "</div>
                <div class=\"prix\">";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["carte"], "prix", [], "any", false, false, false, 23), "html", null, true);
            echo " euros</div>
                </div>
                  
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['carte'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "        
                </div>
            
    
        
  

    
";
    }

    public function getTemplateName()
    {
        return "carte/listeproduits.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 27,  93 => 23,  89 => 22,  82 => 20,  78 => 19,  74 => 17,  70 => 16,  61 => 9,  57 => 8,  51 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "carte/listeproduits.html.twig", "/var/www/html/symfony/DCD/templates/carte/listeproduits.html.twig");
    }
}
