<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* infos/infosaccueil.html.twig */
class __TwigTemplate_ade6eb0421b8798a48612c5cdff61b36 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
 <html>
    <head>
        <meta charset=\"UTF-8\">
         <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
    <link href=\"https://fonts.googleapis.com/css2?family=Libre+Baskerville&display=swap\" rel=\"stylesheet\">
    <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
    <link href=\"https://fonts.googleapis.com/css2?family=Questrial&display=swap\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href='/css/styleinfo.css'  /
 <section>
  <div class=\"infos\">

        <div class=\"googlemap\">
            <iframe class=\"pc\"src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2909.219089684536!2d3.0114733151259623!3d43.18391357914022!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1ac77508ce4eb%3A0xd555a1b79ae98dcc!2s43%20Av.%20Pr%C3%A9sident%20Kennedy%2C%2011100%20Narbonne!5e0!3m2!1sfr!2sfr!4v1637674263179!5m2!1sfr!2sfr\" width=\"850\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" alt=\"plan google map du restaurant\">
            </iframe>
            <iframe class=\"mobile\"src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2909.219032499131!2d3.0114740151259536!3d43.18391477914023!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1ac77508ce4eb%3A0xd555a1b79ae98dcc!2s43%20Av.%20Pr%C3%A9sident%20Kennedy%2C%2011100%20Narbonne!5e0!3m2!1sfr!2sfr!4v1638022666667!5m2!1sfr!2sfr\" width=\"400\" height=\"300\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" alt=\"plan google map du restaurant\">
            </iframe>
        </div>

        <div class=\"coordonnees\">
            <div class=\"text\">
               
            </div>
            <div class=\"liste\">
   ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["infos"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
            // line 28
            echo "                <ul>
                     <li>";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "description", [], "any", false, false, false, 29), "html", null, true);
            echo "</li>
                    <li>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "adresse", [], "any", false, false, false, 30), "html", null, true);
            echo "</li>
                    <li><a href=\"tel:";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "telephone", [], "any", false, false, false, 31), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "telephone", [], "any", false, false, false, 31), "html", null, true);
            echo "</a></li>
                    <li><a href=\"mail:";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "mail", [], "any", false, false, false, 32), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["info"], "mail", [], "any", false, false, false, 32), "html", null, true);
            echo "</a></li>
                </ul>
            </div>
        </div>
    </div>

  ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 39
            echo "            <tr>
                <td colspan=\"6\">no records found</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "

</section>

  ";
    }

    public function getTemplateName()
    {
        return "infos/infosaccueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 43,  101 => 39,  87 => 32,  81 => 31,  77 => 30,  73 => 29,  70 => 28,  65 => 27,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "infos/infosaccueil.html.twig", "/var/www/html/symfony/DCD/templates/infos/infosaccueil.html.twig");
    }
}
