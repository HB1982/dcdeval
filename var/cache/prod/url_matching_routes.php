<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/carte' => [[['_route' => 'app_carte_index', '_controller' => 'App\\Controller\\CarteController::index'], null, ['GET' => 0], null, true, false, null]],
        '/carte/new' => [[['_route' => 'app_carte_new', '_controller' => 'App\\Controller\\CarteController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/' => [[['_route' => 'index_index', '_controller' => 'App\\Controller\\IndexController::index'], null, ['GET' => 0], null, false, false, null]],
        '/infos' => [[['_route' => 'app_infos_index', '_controller' => 'App\\Controller\\InfosController::index'], null, ['GET' => 0], null, true, false, null]],
        '/infos/new' => [[['_route' => 'app_infos_new', '_controller' => 'App\\Controller\\InfosController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/infosaccueil' => [[['_route' => 'app_infosaccueil', '_controller' => 'App\\Controller\\InfosaccueilController::index'], null, null, null, false, false, null]],
        '/menu' => [[['_route' => 'app_menu', '_controller' => 'App\\Controller\\MenuController::index'], null, null, null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/reservation' => [[['_route' => 'app_reservation_index', '_controller' => 'App\\Controller\\ReservationController::index'], null, ['GET' => 0], null, true, false, null]],
        '/reservation/new' => [[['_route' => 'app_reservation_new', '_controller' => 'App\\Controller\\ReservationController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/header' => [[['_route' => 'app_header', '_controller' => 'App\\Controller\\headercontroller::header'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/carte/([^/]++)(?'
                    .'|(*:25)'
                    .'|/edit(*:37)'
                    .'|(*:44)'
                .')'
                .'|/infos/([^/]++)(?'
                    .'|(*:70)'
                    .'|/edit(*:82)'
                    .'|(*:89)'
                .')'
                .'|/reservation/([^/]++)(?'
                    .'|(*:121)'
                    .'|/edit(*:134)'
                    .'|(*:142)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        25 => [[['_route' => 'app_carte_show', '_controller' => 'App\\Controller\\CarteController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        37 => [[['_route' => 'app_carte_edit', '_controller' => 'App\\Controller\\CarteController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        44 => [[['_route' => 'app_carte_delete', '_controller' => 'App\\Controller\\CarteController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        70 => [[['_route' => 'app_infos_show', '_controller' => 'App\\Controller\\InfosController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        82 => [[['_route' => 'app_infos_edit', '_controller' => 'App\\Controller\\InfosController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        89 => [[['_route' => 'app_infos_delete', '_controller' => 'App\\Controller\\InfosController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        121 => [[['_route' => 'app_reservation_show', '_controller' => 'App\\Controller\\ReservationController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        134 => [[['_route' => 'app_reservation_edit', '_controller' => 'App\\Controller\\ReservationController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        142 => [
            [['_route' => 'app_reservation_delete', '_controller' => 'App\\Controller\\ReservationController::delete'], ['id'], ['POST' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
